  $(function() {
    
    var currentUser = {};
    
    function renderLog() {
        var arr = [{
          value:template

        }]
        
        $(".register").html("")
        
        var template = $("<form id='8' class='credentials-login'>" +
                            "<div  class='log-div'>" +
                                "<div class='log-label-input'>" +
                                  "<label> User name </label>" +
                                  "<input type='text' class='user-name log-input' placeholder='User name'>" +
                                "</div>" +
                                "<div class='log-label-input'>" +
                                  "<label> Password </label>" +
                                  "<input type='password' class='password log-input' placeholder='Password'>"+
                                "</div>" +
                              "<div class='log-btn'>" +
                                  "<div class='rem-check'>" + 
                                    "<input type='checkbox' class='remember-checkbox'>" +
                                    "<span class='remember-span'> Remember me </span>" +
                                  "</div>" +
                                "<span class='forgot'> Forgot <a href='#'>password </a> ? </span>" +
                              "</div>" +
                              "<input type='button' class='validate-login' value='Login'>" +
                            "</div>" +
                        "</form>");

          $(".register").append(template);
            

    }

    $(".login-button").on("click", renderLog);

    $(".register").on("keypress", ".log-input", function (event) {
      if (event.which == 13) {
        login();
      }
    });
    
    $(".register").on("click", "#8.credentials-login", function(e) {
      var x = $(e.target).attr("id");
      if (x == 8) {
        $(".credentials-login").remove();        
      }
    });

    $(window).on("keyup", function(e) {
      if (e.keyCode == 27) {
        $(".credentials-login").remove();  
      }
    });


    var currentUser = {};

    function login() {
        var x = $(".user-name").val();
        var y = $(".password").val();
        var user = {
            username: x,
            password: y
        };
            
        $.ajax({
            type: "POST", 
            url: "api/user/login",
            contentType: "application/json",
            data: JSON.stringify(user),
      
            success: function (data) {
              currentUser = data;
              if(x == "" && y == "") {
                alert("Fields must be filled out");
              }
              else { if(currentUser == null) {
                        alert("Invalid user");
                    }
                    else {
                      window.location.assign("./login page/login-page.html")
                    }
              }
            },

            error: function(data) {
              currentUser = data;
            }

        });
    };
      $(".register").on("click", ".validate-login", login);
    
    var users = [];

    function renderReg() {
        var arr = [{
          value:template1
        }];
        
        $(".register").html("")

        var template1 = $("<form id='8' class='credentials-register'>" + 
                            "<div class='cred-div'>" +
                                "<div class='reg-label-input'>" +
                                    "<label> First Name </label>" +
                                    "<input type='text' class='first-name cred-input' placeholder='First Name' required>" +
                                "</div>" +
                                "<div class='reg-label-input'>" +
                                    "<label> Last Name </label>" +
                                    "<input type='text' class='last-name cred-input' placeholder='Last-Name' required>" +
                                "</div>" +
                                "<div class='reg-label-input'>" +
                                    "<label> Birthday </label>" +
                                    "<input type='date' class='register-date cred-input' required>" +
                                "</div>" +
                                "<div class='reg-label-input'>" +
                                    "<label> Address </label>" +
                                    "<input type='text' class='register-address cred-input' placeholder='Address' required>" +
                                "</div>" +
                                "<div class='reg-label-input'>" +
                                    "<label> E-mail </label>" +
                                    "<input type='text' class='register-email cred-input' placeholder='Email' required>" +
                                "</div>" +
                                "<div class='reg-label-input'>" +
                                    "<label> Username </label>" +
                                    "<input type='text' class='register-username cred-input' placeholder='Username' required>" +
                                "</div>" +
                                     "<div class='reg-label-input'>" +
                                    "<label> Password </label>" +
                                    "<input type='password' class='register-password cred-input' placeholder='Password' required>" +
                                "</div>" +
                                "<div class='reg-buttons'>"+
                                  "<input type='submit' class='validate-register' value='Register'>" +
                                  "<input type='button' class='cancel-register' value='Cancel'>" +
                                "</div>" +
                            "</div>" +
                          "</form>");

      $(".register").append(template1);
    }

    $(".register-button").on("click", renderReg);

    var users = [];


    function register() {
      var user = {
        firstName: String,
        lastName: String,
        dateOfBirth: Date,
        username: String,
        password: String,
        userRole: String, 
        address: String,
        email: String
      }

      user.firstName = $(".first-name").val();
      user.lastName = $(".last-name").val();
      user.dateOfBirth = $(".register-date").val();
      user.username = $(".register-username").val();
      user.password = $(".register-password").val();
      user.address = $(".register-address").val();
      user.email = $(".register-email").val();
  
      var dir = "./api/user/register";
      var verify = false;
      var inputs = [];

      $.ajax({
        type: "GET",
        url: "api/users",
        contentType: "application/json",
        data: "",

        success: function(data) {
          users = data;
        }
      });
      
          $(".reg-label-input input").each(function(){
              inputs.push($(this).val());
          });

          for(var i = 0; i <inputs.length; i++) {
            if(inputs[i] === "") {
              verify = true;
              alert("Fields must be filled out");
              break;
            }
          }
          
          for(var i = 0; i <users.length; i++) {
            if(user.username==users[i].username) {
              alert("User exist");
              verify = true;
              break;
            }
          }

          if(!verify) {
              $.ajax({
                type: "POST", 
                url:dir,
                contentType: "application/json",
                data: JSON.stringify(user),
  
                success: function (data) {
                    users.push(data);
                    console.log(data);
                }
              });
          }
    }

   $(".register").on("click", ".validate-register", register);
   $(".register").on("keypress", ".cred-input", function(e) {
      if (event.which == 13) {
        register();
      }
    });

    $(".register").on("click", ".cancel-register", function() {
      $(".credentials-register").remove();
    });


    $(".register").on("click", "#8.credentials-register", function(e) {
      var x = $(e.target).attr("id");
      if (x == 8) {
        $(".credentials-register").remove();        
      }
    });

    $(window).on("keyup", function(e) {
      if (e.keyCode == 27) {
        $(".credentials-register").remove();  
      }
    });
});
