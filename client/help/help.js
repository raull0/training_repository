$(function() {
	function render() {
		var arr = [{
		value: template
		}]

		$(".help-us").html("")

		var template = $("<div class='container' id='2'>" +
							"<div class='help-a'>" +
								"<h1 class='head'><img src='https://s1.emagst.net/layout/ro/info/uploads/2014/05/contact.png'></img>Contact insurance</h1>" +	
									"<div class='width'>" +
										"<label class='margin'><b>Fax: </b></label>" +
										"<p class='margin'>0361-405-258</p>" +
										"<label class='margin'><b>Telephone: </b></label>" +
										"<p class='margin'>0755206312</p>" +
										"<label class='margin'><b>Email: </b></label>" +
										"<p class='margin'>insurance@springshop.com</p>" +
										"<label class='margin'><b>Schedule: </b></label>" +
										"<p class='margin'>Monday-Sunday: 09-19</p>" +
									"</div>" +
								"<h2 class='head'><img src='https://s1.emagst.net/layout/ro/info/uploads/2014/05/contact.png'>Support 24/7</h2>" +
									"<div class='width'>" +
										"<label class='margin'><b>Fax: </b></label>" +
										"<p class='margin'>0361-205-204</p>" +
										"<label class='margin'><b>Telephone: </b></label>" +
										"<p class='margin'>0758423425</p>" +
										"<label class='margin'><b>Schedule: </b></label>" +
										"<p class='margin'>Monday-Sunday: 24/24</p>" +
									"</div>" +
								"<h4 class='head'><img src='https://s1.emagst.net/layout/ro/info/uploads/2014/05/contact.png'>Contact service</h4>" +
									"<div class='width'>" +
										"<label class='margin'><b>Telephone: </b></label>" +
										"<p class='margin'>0741548597</p>" +
										"<label class='margin'><b>Schedule: </b></label>" +
										"<p class='margin'>Monday-Friday: 08-20</p>" +
										"<p class='margin'>Saturday: 10-14</p>" +
									"</div>" +
							"</div>" +
						"</div>");

		$(".help-us").append(template);
	}

	$(".help").on("click", render);

	$(".help-us").on("click", "#2.container", function(e) {
      var x = $(e.target).attr("id");
      if (x == 2) {
        $(".container").remove();        
      }
    });

    $(window).on("keyup", function(e) {
      if (e.keyCode == 27) {
        $(".container").remove();  
      }
    });
})