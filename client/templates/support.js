$(function () {
    
    var $container = $(".main-container");
    var $footer = $("footer");

    $footer.on("click", ".support-a", function() {
        $container.html("");
        var supportTemplate = $(
        "<div class='support-div'>" +
                "<span class='support-title'>Client support 24/7</span>" +
                "<hr class='support-hr'>" +
                "<span class='support-text'>" +
                    "If you can not find the answer to your questions below, you can always call us. Our team of operators is ready to provide support 24 hours a day every day of the week." +
                "</span>" +
                "<span class='support-text'> T*: 021 200.52.00 </span>" +
				"<span class='support-text'> M*: 0722.25.00.00 </span>" +
				"<span class='support-text'> Program: Monday - Sunday: 24/24  </span>" +
				"<span class='support-text'> * normal telephone lines </span>" +
                "<span class='support-subtitle'>" +
                    "Questions you have before you order" +
                "</span>" +
                "<span class='support-spantitle'>" +
                    "How can I find out if a product is in stock in a showroom? " +
                "</span>" +
                "<span class='support-text'>" +
                    "To find out if in a category we have products that are in stock in the eMAG showrooms, follow these steps:" +
					"1. Select the category and subcategory you are interested in to see the product list " +
					 "2. Once you have arrived in the product list, follow the filters menu From the left, unfolding to the end of it; The 'Available in Showroom' filter is positioned there. " +
                "</span>" +
                "<span class='support-spantitle'>" +
                    "I received a gift card in a campaign and tried to use it for an order, but it did not apply. Why? " +
                "</span>" +
                "<span class='support-text'>" +
                    "Most often, you can not apply a gift card won in a campaign on a purchase because the product you want to buy is not part of the campaign category. So, check if the product you choose is or not part of the campaign category." +
					"If the product is part of the campaign category and you are unable to apply the gift card for payment, please contact us at 021 200.52.00 / 0722.25.00.00." +
				"</span>" +
				"<span class='support-spantitle'>" +
					"Why can not I pay my order online with my card? " +
				"</span>" +
				"<span class='support-text'>" +
					"Check once again that you have correctly entered your card data. Incorrect card insertion is the most common reason why card payment does not work. If you entered your card correctly and you are not able to pay, please call us on 021 200.52.00 / 0722.25.00.00." +
				"</span>" +
				"<span class='support-spantitle'>"+
					"What does the product resell? "+
				"</span>"+
				"<span class='support-text'>"+
					"Re-seized products are products that have been exposed to shelves in showrooms or have been tested by customers. They are perfectly functional products, which, before being put on sale, have been tested by technicians. The status of each reshipped product is described in the product page. Re-seized products benefit from warranty."+
					"Please keep in mind that if resealed products, these n u receive any promotions, gifts that accompany new products. The shipping service in case you decide to return the reshipped product is FREE."+
					"To have no doubts about the reshipped product purchased, we provide you with the service to open the package at delivery. Open the package in the presence of the courier and, if you are not happy with the condition of the product, you can refuse it."+
					"If you choose to pick up the re-seized product in the showroom, you can check it at the sample bench and refuse if you do not like its look or how it works."+
				"</span>"+
				"<span class='support-subtitle'>"+
					"Questions about the order"+
				"</span>"+
				"<span class='support-spantitle'>"+
					"I booked an order and want to pay it online. How am I doing? "+
				"</span>"+
				"<span class='support-text'>"+
					"If after you registered your order you have changed your mind about the payment method and you want to pay it with the online card, please call us at 021 200.52.00 / 0722.25.00.00 and we will send you a link for making the online payment."+
				"</span>"+
				"<span class='support-spantitle'>"+
					"I recorded an order and changed my mind, I want another product. How am I doing? "+
				"</span>"+
				"<span class='support-text'>"+
					"If you have not yet received the product you ordered and you do not want it anymore, please call us at 021 200.52.00 / 0722.25.00.00. "+
					"If you have already received the product, fill in and send us the return form."+
				"</span>"+
            "</div>");
            
            $container.append(supportTemplate);
    })

});