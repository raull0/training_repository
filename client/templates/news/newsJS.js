$(function () {

	window.news = [];
	// NEED USER AFTER LOGIN
	user = {
		name: "dwea",
		userRole: "adminj"
	}

	var news = [];

	$.ajax({
		type: "GET",
		url: "/api/news",
		contentType: "application/json",
		data: "",
		success: function (data) {
			news = data;
			render();
		}
	});

	$(".news").append(news);

	function render() {

		$(".newNews").remove();

		for (var i = 0; i < news.length; i++) {
			var newNewsTemp = $(
				"<div class='newNews'>" +
				"<div class='title'>" +
				"<span class='news-title'>" + news[i].title + "</span>" +
				"<span class='date'>" + news[i].date + "</span>" +
				"</div>" +
				"<div class='content'>" +
				"<p>" + news[i].content + "</p>" +
				"<span class='name'>" + "posted by " + news[i].postedBy + "</span>" +
				"</div>" +
				"<hr>" +
				"</div>"
			)
			$(".news").append(newNewsTemp);
		}
	}

	function addNewPost() {
		$(".start-page .news .newPost.delete").remove();
		var newPost = $(
			"<div class='newPost delete'>" +
			"<label>Title</label>" +
			"<input class='title' type=\"text\" >" +
			"<label>Content</label>" +
			"<textarea rows=\"4\" cols=\"50\"></textarea>" +
			"<input class='button' type='button' value='post'>" +
			"</div>"
		)
		$(".start-page .news .newPost").append(newPost);
	}

	if (user.userRole == "admin") {
		$(".news").on("click", ".button", postInDB);
	}
	function postInDB() {

		var title = $(".start-page .news .newPost .title").val();
		var content = $(".start-page .news .newPost textarea").val();
		var date = Date.now();
		var news1 = {
			title: title,
			content: content,
			postedBy: user.name,
			date: date
		}
		console.log(news);
		$.ajax({
			type: "POST",
			url: "/api/new/post",
			contentType: "application/json",
			data: JSON.stringify(news1),
			success: function (data) {
				news.push(news1);
				render(user.userRole);
			}
		});
	}


});