$(function() {

	function render() {
		var arr = [{

			value:template
		}]
	}

	$(".container-footer").html("");

	render();

	var template = $("<footer>" +
			"<div class='footer-above'>" +
				"<div class='footer-container'>" +
					"<div class='footer-div'>" +
							"<span class='know-span'> Get to know us </span>" +
							"<span class='footer-span'> <a href='#' target='_blank' class='footer-a'> Contact </a></span>" +
							"<span class='footer-span map'>" +
								"<a class='footer-a' href='https://www.google.ro/maps/place/Alpha+Business+Center/@46.7508914,23.5693463,17z/data=!3m1!4b1!4m5!3m4!1s0x47490e77c0451b5d:0x5d08b64947bc6341!8m2!3d46.7508914!4d23.571535' target='_blank'>On the map </a><i class='fa fa-map-marker map-icon' aria-hidden='true'></i>" +
							"</span>" +
							"<span class='footer-span'>Around the web" +
								"<a href='https://www.facebook.com/springtechco/' target='_blank'><i class='fa fa-facebook facebook-icon' aria-hidden='true'></i></a>" +
								"<a href='https://twitter.com/springtechco' target='_blank'><i class='fa fa-twitter twitter-icon' aria-hidden='true'></i></a>" +
							"</span>" +
					"</div>" +

					"<div class='footer-div'>" +
						"<span class='support-span'>Client support </span>" +
						"<span class='footer-span'> <a href='#' target='_blank' class='footer-a'>24/7 Support </a></span>" +
						"<span class='footer-span'> <a href='#' target='_blank' class='footer-a'>Returns and Replacements </a></span>" +
						"<span class='footer-span'> <a href='#' target='_blank' class='footer-a'>Shipping rates and policies </a></span>" +
						"<span class='footer-span'> <a href='#' target='_blank' class='footer-a'>Frequent questions </a></span>" +
					"</div>" +

					"<div class='footer-div'>" +
						"<span class='let-us-span'>Let us help you </span>" +
						"<span class='footer-span'><a href='#' target='_blank' class='footer-a'>Your account </a></span>" +
						"<span class='footer-span'><a href='#' target='_blank' class='footer-a'>Your orders </a></span>" +
						"<span class='footer-span'><a href='#' target='_blank' class='footer-a'>Help </a></span>" +
					"</div>" +
				"</div>" +
			"</div>" +

			"<div class='copyright'>" +
				"<span> Copyright <i class='fa fa-copyright' aria-hidden='true'></i> 2017 SpringTechNewbies </span>" +
			"</div>" +
		"</footer>"); 

	$(".container-footer").append(template); 
});