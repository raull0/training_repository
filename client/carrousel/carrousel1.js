$(function() {
  
  var arr = [{ path: "https://icdn2.digitaltrends.com/image/samsung-galaxy-wide_0018-720x480-c.jpg?ver=1",
    description: "iphone nu stiu deca nici nu stiu daca e iphone stocare: bla bla ecran nucare: bla bla ecran nucare: bla bla ecran nucare: bla bla ecran nucare: bla bla ecran nucare: blaa bla ecran nucare: bla bla ecran nucare: bla bla ecran nucare: blaa bla ecran nucare: bla bla ecran nucare: bla bla ecran nucare: bla bla ecran nu stiu de care", price: "10.000$"}, 
    { path: "https://i0.wp.com/phonesinc.com/blog/wp-content/uploads/2017/04/Samsung-Galaxy-S8-review.jpg?fit=720%2C480&ssl=1"},
    { path:"http://www.buyiphoneonline.com/wp-content/uploads/2017/05/apple-iphone-7-32gb.jpg"},
    { path:"http://laptopdes.com/wp-content/uploads/2015/04/best-small-i5-laptop.jpg"},
    { path:"https://www.techbes.com/file/2017/05/1495130278_707_lenovos-updated-laptop-2-in-1-portfolio-includes-its-most-powerful-mobile-gaming-rig-to-date-digitaltrends.jpg"},
    { path:"http://cdn.windowsreport.com/wp-content/uploads/2016/04/Lenovo-Yoga-900-e1460727162457.jpg"},
    { path:"https://icdn5.digitaltrends.com/image/origin-millenium-pc-2016-ondesk1-720x480.jpg"},
    { path:"https://img.digitaltrends.com/image/pc-fanless-calyos-03-720x720.jpg"},
    //{ path:"https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg"}
  ];
  var rArrow = 1;
  var numberOfPhotos = arr.length;
  var currentPhoto = -1;

  rightArrow();

  $(".right.arrow").on("click", rightArrow);
  $(".left.arrow").on("click", leftArrow);

  function rightArrow() {
    var photosOnSlide = 3;
    $(".image-container").remove();
    
    while(photosOnSlide > 0) { 
      if (++currentPhoto >= arr.length) {
        currentPhoto = 0;
      }
      var template = $("<div class='image-container'>" +
                        "<img class='image' src='" + arr[currentPhoto].path + "'>" +
                        "<text class='description'>" + arr[currentPhoto].description + "</text>" + "<text>Price: " + arr[currentPhoto].price + "</text>" +
                      "</div>");
      $(".slideshow-container").append(template);
      photosOnSlide--;
    }
  }
  
  function leftArrow() {
    var photosOnSlide = 3;
    $(".image-container").remove();

    var aux = currentPhoto;
    currentPhoto -= 6;

    while (photosOnSlide > 0) {
      if (++currentPhoto >= arr.length) {
        currentPhoto = 0;
      } else if (currentPhoto < 0) {
        currentPhoto = arr.length + currentPhoto;
      }
      var template = $("<div class='image-container'>" +
                        "<img class='image' src='" + arr[currentPhoto].path + "'>" +
                        "<text class='description'>" + arr[currentPhoto].description + "</text>" + "<text>Price: " + arr[currentPhoto].price + "</text>" +
                      "</div>");
      $(".slideshow-container").append(template);
      photosOnSlide--;
    }
  }
});