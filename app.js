const Express = require("express");
const Mongoose = require("mongoose");
const BodyParser = require("body-parser");
const Bluebird = require("bluebird");

var app = Express();

app.use(BodyParser.json());
app.use("/",Express.static(__dirname + "/client"));

Mongoose.Promise = Bluebird;
Mongoose.connect("localhost", "training_repository", function() {
	var modules = ["user", "news", "product", "category"];

	modules.forEach(function(item, index) {
		require("./server/" + item + "/" + item + ".model")(Mongoose);
	});

	modules.forEach(function(item, index) {
		require("./server/" + item + "/" + item + ".router")(app, Mongoose);
	});

	console.log("Mongoose loaded");
});

app.listen(4001, function() {
	console.log("Server Started");
});

// Mongoose.connect("localhost", "training_repository", function() {
// 	var userSchema = Mongoose.Schema({
// 		firstName: String,
// 		lastName: String,
// 		dateOfBirth: Date,
// 		username: String,
// 		password: String,
// 		user_role: String, // client or manager
// 		address: String,
// 		email: String
// 	});



// 	var orderSchema = Mongoose.Schema({
// 		clientId: Number,
// 		sent: Boolean,// 0 if still buying , 1 after order button is pressed
// 		products: [],
// 		price: Number

// 	});

// 	var User = Mongoose.model("users",userSchema);
// 	var Product = Mongoose.model("products", productSchema);
// 	var Order = Mongoose.model("orders", orderSchema);
// // ------------------------------Get Orders ------------------------------
// 	app.get("/orders", function(request, response) {
// 		Order.find(function(error,result) {
// 			response.status(200).json(result);
// 	 	});
// 	});

// // ------------------------------Create Order ----------------------------
// 	app.post("/order/create",function(request, response) {
// 		var order =  new Order({
// 			clientId: request.body.clientId,
// 			price: request.body.price,
// 			sent: request.body.sent
// 		});

// 		order.save(function(error, result){
// 			console.log(error, result);
// 			response.status(200).json(result);
// 		});
// 	});
// // --------------------------Find and Update Order ----------------------
// 	app.put("/order/update", function(request, response){
// 		Order.findOneAndUpdate({_id:request.body._id}, { $push: {
// 			products: request.body.products,
// 		}}, function(error, result) {
// 				response.status(200).json(result);
// 		});
// 	});

// })
