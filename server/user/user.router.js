const UserManager = require("./user.manager");

module.exports = function(app, Mongoose) {
	var userManager = new UserManager(Mongoose);

	app.get("/api/users", function(request, response) {
		userManager.getUsers(function(users) {
			response.status(200).json(users);
		}, function(error) {
			response.status(500).json(error);
		});
	});

	app.post("/api/user/login", function(request, response) {
		console.log("REQUEST: ", request);
		userManager.getUser(request.body.username, request.body.password, function(data) {
			response.status(200).json(data);
		}, function(error) {
			response.status(500).json(error);
		})
	})

	app.post("/api/user/register", function(request, response) {
		userManager.createUser(request.body, function(data) {
			response.status(200).json(data);
		}, function(error) {
			response.status(500).json(error);
		});
	});

	app.delete("/api/user/remove", function(request, response) {
		userManager.deleteUser(request.body.id, function(data) {
			response.status(200).json(data);
		}, function(error) {
			response.status(200).json(error);
		});
	});

};