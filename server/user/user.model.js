module.exports = function(Mongoose) {
	var userSchema = Mongoose.Schema({
		firstName: String,
		lastName: String,
		dateOfBirth: Date,
		username: String,
		password: String,
		userRole: String, // client or manager
		address: String,
		email: String
	}, {
		toJSON: {
			transform: function(doc, ret) {
				ret.id = ret._id;

				delete ret._id;
				delete ret.password;
			}
		}
	});

	var User = Mongoose.model("User",userSchema);
};