module.exports = function(Mongoose) {
	var User = Mongoose.models.User;

	this.getUsers = function(success, fail) {
		User.find(function(error, result) {
			error ? fail(error) : success(result);
		});
	};

	this.getUser = function(username, password, success, fail) {
		User.findOne({ 
			username: username,
			password: password
			}, function(error, result) {
				error ? fail(error) : success(result);
			});
	};

	this.createUser = function(user, success, fail) {
		var newUser = new User({
			firstName:  user.firstName,
			lastName: user.lastName,
			dateOfBirth: user.dateOfBirth,
			username: user.username,
			password: user.password,
			userRole: user.userRole,
			address: user.address,
			email: user.email
		});

		newUser.save(function(error, result) {
			error ? fail(error) : success(result);
		});
	}

	this.deleteUser = function(id, success, fail) {
		User.remove({ _id: id }, function(error, result) {
				error ? fail(error) : success(result);
			});
	}
	return this;
};