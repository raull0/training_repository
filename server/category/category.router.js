const CategoryManager = require("./category.manager");

module.exports = function(app, Mongoose) {

	var categoryManager = new CategoryManager(Mongoose);

	app.post("/api/category/add", function(request, response) {
		categoryManager.addCategory(request.body, function(data) {
			response.status(200).json(data);
		}, function(error) {
			response.status(500).json(error);
		})
	})
}