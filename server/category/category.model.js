module.exports = function(Mongoose) {
	var categorySchema = new Mongoose.Schema({
		title: String
	}, {
		toJSON: {
			transform: function(doc, ret) {
				ret.id = ret._id;

				delete ret._id;
			}
		}
	})

	var Category = Mongoose.model("Category", categorySchema);
}