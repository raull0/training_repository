 module.exports = function(Mongoose) {
 	var Category = Mongoose.models.Category;

 	this.addCategory = function(title, success, fail) {
 		var category = new Category({
 			title: title
 		});

 		category.save(function(error, result) {
 			error ? fail(error) : success(result);
 		});
 	};
 };