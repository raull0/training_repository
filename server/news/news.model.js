module.exports = function(Mongoose) {
	var newsSchema = Mongoose.Schema({
		date: Date,
		content: String,
		postedBy: String,
		title: String
	}, {
		toJSON: {
			transform: function(doc, ret) {
				ret.id = ret._id;

				delete ret._id;
			}
		}
	});
	var News = Mongoose.model("News", newsSchema);
}
