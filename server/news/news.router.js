const NewsManager = require("./news.manager");

module.exports = function(app, Mongoose) {
	var newsManager = new NewsManager(Mongoose);

	app.get("/api/news", function(request, response) {
		newsManager.getNews(function(data) {
			response.status(200).json(data);
		}, function(error) {
			response.status(500).json(error);
		})
	})

	app.post("/api/new/post", function(request, response) {
		newsManager.postNews(request.body, function(data){
			response.status(200).json(data);
		}, function(error) {
			response.status(500).json(error);
		})
	})

	app.delete("/api/news", function(request, response) {
		newsManager.deleteNews(function(data) {
			response.status(200).json(data);
		}, function(error) {
			response.status(500).json(error);
		})
	})
}