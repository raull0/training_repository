module.exports = function(Mongoose) {
	var News = Mongoose.models.News;

	this.getNews = function(success, fail) {
		News.find().sort({date:-1}).limit(4).exec( function(error, result) {
			error ? fail(error) : success(result);
		});
	};

	this.postNews = function(news, success, fail) {
		var newNews = new News({
			date: news.date,
			content: news.content,
			postedBy: news.postedBy,
			title: news.title
		});
		newNews.save(function(error, result) {
			error ? fail(error) : success(result);
		});
	};

	this.deleteNews = function(success, fail) {
		News.remove(function(error, result) {
			error ? fail(error) : success(result);
		});
	};
};
