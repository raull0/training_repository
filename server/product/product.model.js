module.exports = function(Mongoose) {
	var productSchema = Mongoose.Schema({
		category: {
			type: Mongoose.Schema.Types.ObjectId,
			ref: "Category"
		},
		name: String,
		description: String,
		stock: Number,
		price: Number,
		path: String,
		sale: {
			type: Number,
			default: 0
		}
	}, {
		toJSON: {
			transform: function(doc, ret) {
 				ret.id = ret._id;
 
 				delete ret._id;
 			}
 		}
 	});
 
 	var Product = Mongoose.model("Product",productSchema);
 };
