module.exports = function(Mongoose) {
	var Product = Mongoose.models.Product;

	this.getProducts = function(success, fail) {
		Product.find(function(error, result) {
			error ? fail(error) : success(result);
		});
	};

	this.getProduct = function(id, success, fail) {
		Product.findOne({_id: id}, function(error,result) {
			error ? fail(error) : success(result);
		});
	};

	this.addProduct = function(product, success, fail) {
		var product = new Product({
			name: product.name,
			description: product.description,
			path: product.path,
			stock: product.stock,
			price: product.price
		});

		product.save(function(error, result) {
			error ? fail(error) : success(result);
		});
	};

	this.updateProduct = function(id, product, success, fail) {
		Product.findOneAndUpdate({_id: id}, {
			$set: product
		}, function(error,result) {
			error ? fail(error) : success(result);
		});
	};


	this.deleteProduct = function(id, success, fail) {
		Product.findOneAndRemove({_id: id}, function(error,result) {
			error ? fail(error) : success(result);
		});
	};

	this.deleteProducts = function(success, fail) {
		Product.remove(function(error, result) {
			error ? fail(error) : success(result);
		})
	}
}