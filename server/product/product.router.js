const ProductManager = require("./product.manager")

module.exports = function(app, Mongoose) {
	var productManager = new ProductManager(Mongoose);

	app.get("/api/products", function(request, response) {
		productManager.getProducts(function(data) {
			response.status(200).json(data);
 		}, function(error) {
 			response.status(500).json(error);
 		});
	});

	app.post("/api/product/add", function(request, response) {
		productManager.addProduct(request.body,function(data) {
			response.status(200).json(data);
 		}, function(error) {
 			response.status(500).json(error);
 		});
	});

	app.put("/api/product/:id", function(request, response) {
		productManager.updateProduct(request.params.id, request.body, function(data) {
			response.status(200).json(data);
 		}, function(error) {
 			response.status(500).json(error);
 		});
	}); 

	app.delete("/api/product/:id", function(request, response) {
		productManager.deleteProduct(request.params.id, function(data) {
			response.status(200).json(data);
 		}, function(error) {
 			response.status(500).json(error);
 		});
	}); 

	app.delete("/api/products", function(request, response) {
		productManager.deleteProducts(function(data) {
			response.status(200).json(data);
 		}, function(error) {
 			response.status(500).json(error);
 		});
	}); 
}